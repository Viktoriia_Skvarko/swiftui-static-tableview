//
//  ContentView.swift
//  SwiftUIStaticTableView
//
//  Created by Viktoriia Skvarko on 14.08.2021.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        List {
            CellContentView()
            CellContentView()
            CellContentView()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
