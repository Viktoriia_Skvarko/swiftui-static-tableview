//
//  CellContentView.swift
//  SwiftUIStaticTableView
//
//  Created by Viktoriia Skvarko on 14.08.2021.
//

import SwiftUI

struct CellContentView: View {
    var body: some View {
        VStack() {
            HStack() {
                Image("audiA8")
                    .resizable()
                    .frame(width: 80, height: 80)
                    .clipShape(Circle())
                Text("Audi A8")
                Spacer()
            }.padding()
            HStack() {
                Text("The Audi A8 is a four-door, full-size, luxury sedan manufactured and marketed by the German automaker Audi since 1994. The first two generations employed the Volkswagen Group D platform, with the current generation deriving from the MLB platform.")
            }.padding()
        }
    }
}

struct CellContentView_Previews: PreviewProvider {
    static var previews: some View {
        CellContentView()
    }
}
