//
//  SwiftUIStaticTableViewApp.swift
//  SwiftUIStaticTableView
//
//  Created by Viktoriia Skvarko on 14.08.2021.
//

import SwiftUI

@main
struct SwiftUIStaticTableViewApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
